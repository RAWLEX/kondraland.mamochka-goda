/**
 * Created by RiskyWorks on 05.02.2018.
 */

const NODE_ENV = process.env.NODE_ENV;

console.log("Script running in " + NODE_ENV + " mode");
console.log("isDev: " + isDev());

const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const autoprefixer = require('autoprefixer');
const imagemin = require('imagemin');
const imageminPngquant = require('imagemin-pngquant');
const os = require('os');
const ifaces = os.networkInterfaces();

let paths = {
    build: '/build',
    buildRoot: './build',
    buildImages: './assets/images/',
    buildFonts: './assets/fonts/',
    buildStyles: './assets/styles/',
    buildDocs: './docs/',
    appImages: './app/assets/images/',
    appComponents: './app/components/',
    appStatic: './app/static/',
    pages: './pages'
};

function isDev() {
    return NODE_ENV.localeCompare("development") === 0;
}

function getLocalIpAddress() {
    let localIpAddress = 'localhost';


    let targetIfaces = ifaces['Ethernet'] !== undefined ? ifaces['Ethernet'] :
        ifaces['Беспроводная сеть 2'] !== undefined ? ifaces ['Беспроводная сеть 2'] : undefined;

    if(!targetIfaces) {
        console.log('[x] error in detecting local ip address');
        return localIpAddress;
    }

    let iface = targetIfaces.find((iface) => {
       return iface.family === 'IPv4';
    });

    if(!iface) {
        console.log('[x] error in detecting local ip address');
        return localIpAddress;
    }

    localIpAddress = iface.address;

    console.log(`[x] local ip address is ${localIpAddress}\n`);

    return localIpAddress;
}

function getPlugins() {
    let plugins = [
        new webpack.DefinePlugin({
            NODE_ENV:   JSON.stringify(NODE_ENV),
            LANG:       JSON.stringify('ru')
        }),
        new CleanWebpackPlugin(
            paths.buildRoot, {
                verbose: true
            }),
        new ExtractTextPlugin({
            filename: isDev() ?
                paths.buildStyles +  '[name].[hash].css' :
                paths.buildStyles +  '[name].css'
        }),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'commons',
        }),
        new HtmlWebpackPlugin({
            chunks: ['jug', 'commons'],
            template: paths.pages + '/jug/index.pug',
            filename: 'jug.html'
        }),
        new HtmlWebpackPlugin({
            chunks: ['sibir', 'commons'],
            template: paths.pages + '/sibir/index.pug',
            filename: 'sibir.html'
        }),
        new HtmlWebpackPlugin({
            chunks: ['ural', 'commons'],
            template: paths.pages + '/ural/index.pug',
            filename: 'ural.html'
        }),
        new HtmlWebpackPlugin({
            chunks: ['select_region', 'commons'],
            template: paths.pages + '/select-region/select-region.pug',
            filename: 'index.html'
        }),
        new CopyWebpackPlugin([
            {from: paths.appComponents + '/php.email-notifier/build/email', to: './email'},
            {from: paths.appComponents + '/php.email-notifier/build/vendor', to: './vendor'},
            {from: paths.appComponents + '/php.email-notifier/build/backend/core.php', to: './tc-email-notifier.php'},
        ])
    ];

    if(!isDev()) {
        plugins.push(new UglifyJsPlugin());
        plugins.push(new webpack.LoaderOptionsPlugin({
            minimize: true,
            options: {
                postcss: [autoprefixer]
            }
        }));
    }

    return plugins;
}

module.exports = {

    /* main */
    context: __dirname,
    entry: {
        jug: paths.pages + "/jug/index.js",
        sibir: paths.pages + "/sibir/index.js",
        ural: paths.pages + "/ural/index.js",
        select_region: paths.pages + "/select-region/select-region.js",
    },
    output: {
        path: __dirname + paths.build,
        filename: isDev() ? '[name].[hash].bundle.js' : '[name].bundle.js',
    },

    /* devtools */
    watch: isDev(),
    watchOptions: {
        aggregateTimeout: 100
    },

    devServer: {
        host: getLocalIpAddress(),
        port: 8081,
    },

    devtool: isDev() ? 'cheap-inline-module-source-map' : false,

    /* plugins */
    plugins: getPlugins(),
    module: {
        rules : [ {
            test: /\.js$/,
            loader: 'babel-loader'
        }, {
            test: /\.pug$/,
            loader: 'pug-loader',
            options: {
                pretty: isDev()
            }
        }, {
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'sass-loader'],
                publicPath: '../../'
            })
        }, {
            test: /\.sass$/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'sass-loader'],
                publicPath: '../../'
            })
        }, {
            test: /\.styl/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: ['css-loader', 'stylus-loader'],
                publicPath: '../../'
            })
        }, {
            test: /\.(pdf|doc|docx)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    outputPath: paths.buildDocs
                }
            }]
        },{
            test:/\.(eot|svg|ttf|woff|woff2)$/,
            use: [{
                loader: 'file-loader',
                options: {
                    outputPath: paths.buildFonts
                }
            }]
        },{
            test: /\.ico$/,
            loaders: [
                {
                    loader: 'file-loader',
                    options: {
                        outputPath: paths.buildImages
                    }
                }
            ]
        },{
            test: /\.(png|jpe?g|gif)$/,
            loaders: [
                {
                    loader: 'file-loader',
                    options: {
                        outputPath: paths.buildImages
                    }
                },{
                    loader: 'imagemin-loader',
                    options: {
                        enabled: !isDev(),
                        plugins: [
                            {
                                use: 'imagemin-pngquant',
                                options: {
                                    quality: '50-60'
                                }
                            }
                        ]
                    }
                },{
                    loader: "image-maxsize-webpack-loader",
                    options: {
                        "max-width": 800,    // sets max-width for gm/imagemagick scaling, in pixels
                        "max-height": 600,   // sets max-height for gm/imagemagick scaling, in pixels
                        "useImageMagick": false // defaults to false, this controls the usage of imagemagick or graphicsmagick, when false, graphicsmagick is used
                    }
                }
            ]
        }
        ]
    }
};

