require('../style-chunk-1/common.styl');
require('./index.pug');
require('./index.styl');
require('../../node_modules/bootstrap-styl');

import {mobileMenuInit, anchorLinksInit, initStickyNavbar} from '../../blocks/navbar/navbar';
import {initParallax} from '../../blocks/header-2/header'
import {initializeClock} from '../../blocks/countdown/countdown'
import {initGallery} from '../../blocks/gallery/gallery'
import {runTabs} from '../../blocks/gallery/video-tabs'


import {animateInit as aboutAnimate } from '../../blocks/about/about'
require('waypoints/lib/noframework.waypoints.min');

mobileMenuInit();
anchorLinksInit();
initParallax();

function formatParams(params, append){
    let res = append ? "&" : "?";
    return  res + Object
            .keys(params)
            .map(function (key) {
                return key + "=" + encodeURIComponent(params[key])
            })
            .join("&");
}

let request = obj => {
    return new Promise((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        let fullUrl = obj.url + formatParams(obj.params, false);
        xhr.open("GET", fullUrl);
        if (obj.headers) {
            Object.keys(obj.headers).forEach(key => {
                xhr.setRequestHeader(key, obj.headers[key]);
            });
        }
        xhr.onload = () => {
            if (xhr.status >= 200 && xhr.status < 300) {
                resolve(xhr.response);
            } else {
                reject(xhr.statusText);
            }
        };
        xhr.onerror = () => reject(xhr.statusText);
        xhr.send(obj.body);
    });
};


/* gallery init */
let galleryParams = {
    thumbClass : 'gallery-item',
    version: '5.73',
    numOfImages: 24,
};

initGallery('.plates', galleryParams);
runTabs();

/* region page countdown */
initializeClock('clockdiv');

/* callbackFormInit */
import alertify from '../../node_modules/alertifyjs/build/alertify.min'
import TcEmailNotifier from '../../app/components/php.email-notifier/build/tc-email-notifier';

request({url: window.location,
    params: {
        get_ajax_param : 'rg_google_api_key'
    }})
    .then(answer => {
        alertify.defaults.glossary.title = "Уведомление";
        let fields = [
            {
                name: 'name',
                id: 'name',
                type: 'text',
                placeholder: 'Введите свое имя',
                required: true,
                cssClass: 'name',
                parentClass: 'fields'
            },{
                name: 'email',
                id: 'email',
                type: 'email',
                placeholder: 'Введите свой email',
                required: true,
                cssClass: 'email',
                parentClass: 'fields'
            },{
                name: 'tel',
                id: 'tel',
                type: 'tel',
                placeholder: 'Введите свой телефон',
                required: true,
                cssClass: 'tel',
                parentClass: 'fields'
            },{
                name: 'mode',
                id: 'mode',
                type: 'hidden',
                value: 'call-2'
            },{
                name: 'region',
                id: 'region',
                type: 'hidden',
                value: document.querySelector('.region-select').childNodes[1].childNodes[0].innerText
            },{
                name: 'submit',
                id: 'submit',
                type: 'submit',
                cssClass: 'rounded',
                parentClass: 'buttons'
            }
        ];
        
        let options1 = {
            formId: 'callback1',
            backendUrl: '/tc-email-notifier.php',
            isDebugMode: false,
            addHiddenFields: true,
            addGeolocation: true,
            addRecaptchaButton: false,
            googleMapsApiKey: JSON.parse(answer).data,
            onSuccess: (message) => {  alertify.alert(message); },
            onError: (message) => {  alertify.alert(message); },
        };

        let options2 = {
            formId: 'callback2',
            backendUrl: 'tc-email-notifier.php',
            isDebugMode: false,
            addHiddenFields: true,
            addGeolocation: true,
            addRecaptchaButton: false,
            googleMapsApiKey: JSON.parse(answer).data,
            onSuccess: (message) => {  alertify.alert(message); },
            onError: (message) => {  alertify.alert(message); },
        };

        new TcEmailNotifier(options1, fields);
        new TcEmailNotifier(options2, fields);
    })
    .catch(error => {
        console.log(error);
    });


/* animate.css */
let waypoints = [];

waypoints.push(
    new Waypoint({
        element: document.querySelector('.countdown '),
        handler: (direction) => {
            let element = document.querySelector('.countdown');
            element.classList.add('animated');
            element.classList.add('bounceIn');
        },
        offset: 200
}));

waypoints.push(
    new Waypoint({
        element: document.querySelector('.about'),
        handler: (direction) => {
            aboutAnimate();
        },
        offset: 200
    }
));

waypoints.push(
    new Waypoint({
        element: document.querySelectorAll('.callbackform')[0],
        handler: (direction) => {
            let element = document.querySelectorAll('.callbackform')[0];
            element.classList.add('animated');
            element.classList.add('fadeInDown');
        },
        offset: 200
    }
));

waypoints.push(
    new Waypoint({
        element: document.querySelectorAll('.callbackform')[1],
        handler: (direction) => {
            let element = document.querySelectorAll('.callbackform')[1];
            element.classList.add('animated');
            element.classList.add('bounceIn');
        },
        offset: 200
    }
));

document.querySelectorAll('.plates').forEach((e) => {
   waypoints.push(
       new Waypoint({
           element: e,
           handler: (direction) => {
               e.classList.add('animated');
               e.classList.add('bounceIn');
           },
           offset: 200
       })
   )
});

waypoints.push(
    new Waypoint({
        element: document.querySelector('.plates'),
        handler: (direction) => {
            let element = document.querySelector('.plates');
            element.classList.add('animated');
            element.classList.add('bounceIn');
        },
        offset: 200
    }
));


waypoints.push(
    new Waypoint({
        element: document.querySelector('.slogan'),
        handler: (direction) => {
            let element = document.querySelector('.slogan');
            element.classList.add('animated');
            element.classList.add('bounceIn');
        },
        offset: 200
    }
));


waypoints.push(
    new Waypoint({
        element: document.querySelector('#prise'),
        handler: (direction) => {
            let element = document.querySelector('#prise');
            element.classList.add('animated');
            element.classList.add('fadeInDown');
        },
        offset: 200
    }
));

waypoints.push(
    new Waypoint({
        element: document.querySelector('.other-projects'),
        handler: (direction) => {
            let element = document.querySelector('.other-projects');
            element.classList.add('animated');
            element.classList.add('bounceIn');
        },
        offset: 200
    }
));