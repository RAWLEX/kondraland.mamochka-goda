let expandMenuButton = document.querySelector('.navbar').querySelector('menu').querySelector('.show');
let menu = document.querySelector('.navbar').querySelector('menu').querySelector('ul');
let scrollToElement = require('scroll-to-element');


function anchorLinksInit() {
    document.querySelectorAll('a.scroll').forEach((link) => {
        link.addEventListener('click', (e) => {
            if(e.target.pathname === '/index/' && window.location.pathname !== '/') {
                window.location.href = '/';
                return;
            }

            let targetElement = document.querySelector(`a[name=${e.target.hash.slice(1)}`);
            if(targetElement) {
                scrollToElement(targetElement, {
                    offset: 0,
                    ease: 'in-quad',
                    duration: 1500
                });
            }
        });
    });
}

function mobileMenuInit() {
    let mobileWidth = 800;

    expandMenuButton.addEventListener('click', (e) => {
        menu.classList.toggle('visible');
    });

    window.addEventListener('resize', (e) => {
        if(window.innerWidth >= mobileWidth) {
            menu.classList.add('visible');
        } else {
            menu.classList.remove('visible');
        }
    });

    window.addEventListener('load', (e) => {
        if(window.innerWidth >= mobileWidth) {
            menu.classList.add('visible');
        }
    })
}

function initStickyNavbar() {
    let navbar = document.querySelector('.navbar');
    let sticky = navbar.offsetTop;
    window.addEventListener('scroll', (e) => {
        if(window.pageYOffset >= sticky) {
            navbar.classList.add('sticky');
        } else {
            navbar.classList.remove('sticky');
        }
    });
}

export {anchorLinksInit, mobileMenuInit, initStickyNavbar}