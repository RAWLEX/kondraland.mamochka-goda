import Parallax from 'parallax-js/dist/parallax.min'

function initParallax() {
    let scene = document.querySelector('header').querySelector('.poster').querySelector('.wrapper').querySelector('.scene');
    let parallaxInstance = new Parallax(scene);
}

export {initParallax}