function animateInit() {
    let elements = document.querySelector('.advantages').querySelectorAll('.item');
    elements.forEach((e, i) => {
       e.classList.add('animated');
       e.classList.add('fadeIn');
    });
}

export {animateInit}