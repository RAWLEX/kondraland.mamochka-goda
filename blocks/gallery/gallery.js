import PhotoSwipe from '../../node_modules/photoswipe/dist/photoswipe.min';
import PhotoSwipeUI_Default  from '../../node_modules/photoswipe/dist/photoswipe-ui-default.min';

let _container;
let _galleryParams;

function scriptRequest(url, onSuccess, onError) {
    let scriptOk = false;
    let callbackName = 'cb' + String(Math.random()).slice(-6);

    url += ~url.indexOf('?') ? '&' : '?';
    url += 'callback=' + callbackName;

    window[callbackName] = function(data) {
        scriptOk = true; 
        delete window[callbackName];
        onSuccess(data); 
    };

    function checkCallback() {
        if (scriptOk) return; 
        delete window[callbackName];
        onError(url); 
    }

    let script = document.createElement('script');

    script.onreadystatechange = function() {
        if (this.readyState === 'complete' || this.readyState === 'loaded') {
            this.onreadystatechange = null;
            setTimeout(checkCallback, 0); 
        }
    };

    script.onload = script.onerror = checkCallback;
    script.src = url;

    document.body.appendChild(script);
}

function openPhotoSwipe(e) {
    let pswpElement = document.querySelector('.pswp');

    let options = {
        history: false,
        focus: false,
        showAnimationDuration: 0,
        hideAnimationDuration: 0
    };

    let items = [];

    e.currentTarget.parentNode.querySelectorAll('img').forEach((element) => {
        let item  = {};
        item.src = element.getAttribute('data-full');

        let w = element.getAttribute('data-width') === "undefined" ? 400 : parseInt(element.getAttribute('data-width'));
        let h = element.getAttribute('data-height') === "undefined" ? 400 : parseInt(element.getAttribute('data-height'));

        item.w = w;
        item.h = h;

        items.push(item);
    });

    let gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();

    let nodes = Array.prototype.slice.call(e.currentTarget.parentNode.querySelectorAll('img')),
        targetNode = e.target;

    gallery.goTo(nodes.indexOf(targetNode));
}

function onClick(e) {
    e.preventDefault();
    openPhotoSwipe(e);
}

function onSuccess(data) {
    let dataAlbum = `album${data.response[0].owner_id}_${data.response[0].aid}`;
    let galleryElement = document.querySelector(`${_container}[data-album="${dataAlbum}"]`);

    data.response.forEach((item) => {
        let imgLarge = item.src_xxxbig;

        (!(imgLarge)) ? imgLarge = item.src_big : imgLarge;

        let imgThumb = item.src ? item.src : item.src_small;

        let imgElement = document.createElement('img');

        imgElement.classList.add(_galleryParams.thumbClass);
        imgElement.setAttribute('src', imgThumb);
        imgElement.setAttribute('data-full', imgLarge);
        imgElement.setAttribute('data-width', item.width);
        imgElement.setAttribute('data-height', item.height);
        imgElement.onclick = (e) => { onClick(e) };

        galleryElement.appendChild(imgElement);
    });
}

function onError(url) {
    alert( 'Ошибка при запросе ' + url );
}

function initGallery(container, galleryParams) {
    _galleryParams = galleryParams;
    _container = container;

    let galleries = document.querySelectorAll(_container);

    galleries.forEach((g) => {
        if(g.getAttribute('data-render') === "no") {
            console.log('[x] data-render set to none, skipping');
            return;
        }

        let url = 'https://api.vk.com/method/photos.get?',
            params = {
                owner_id: g.getAttribute('data-album').split('_')[0].slice(5),
                album_id: g.getAttribute('data-album').split('_')[1],
                version: _galleryParams.version,
                rev: true,
                count: _galleryParams.numOfImages,
            };

        let paramsString = Object.entries(params).join('&');
        paramsString = paramsString.replace(new RegExp('\,', 'g'),'=');

        scriptRequest(`${url}${paramsString}`, onSuccess, onError);
    });
}

export {initGallery}



