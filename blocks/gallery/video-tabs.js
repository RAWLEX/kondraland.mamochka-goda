import {Tabs} from 'future-tabs';

function runTabs() {
    let tabsElement = document.querySelector('.gallery').querySelector('.tabs');
    new Tabs(tabsElement);
}

export {runTabs}